一般情况下，配置下这个就够了

.npmrc 
```conf
sass_binary_site=https://npmmirror.com/mirrors/node-sass/
```
个别情况下没有成功匹配nodejs版本，无法下载预编译好的node文件，仍然需要在本机编译的话，可以考虑这样

``` shell
npm i node-sass@npm:sass
```
把node-sass安装为sass，就可以骗过nodejs，从而完成安装和启动项目