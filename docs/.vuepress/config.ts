import { defineUserConfig, viteBundler } from "vuepress"
import { searchProPlugin } from "vuepress-plugin-search-pro"
import { hopeTheme } from "vuepress-theme-hope"

export default defineUserConfig({
  title: "cpf23333的博客",
  description: "想起来就写一下，想不起来就不写了",
  lang: "zh-CN",
  head: [["link", { rel: "icon", href: "/favicon.ico" }]],
  bundler: viteBundler(),
  plugins: [searchProPlugin({ indexContent: true })],
  base: "/",
  theme: hopeTheme({
    hostname: "cpf23333.top",
    navbar: [
      {
        text: "网易云音乐api",
        link: "/neteaseMusic",
        target: "_blank",
        ariaLabel: "网易云音乐api",
      },
    ],
    fullscreen: true,
    toc: true,
    docsBranch: "master",
    navbarLayout: {
      start: ["Brand", "Links"],
      center: [],
      end: ["Search", "Repo", "Outlook"],
    },
    sidebar: [
      {
        text: "vue2",
        link: "/vue2/",
        children: [
          {
            text: "组件的promise化",
            link: "/vue2/componentPromiseify/",
          },
          {
            text: "h函数相关",
            link: "/vue2/hFunction/",
          },
        ],
      },
      {
        text: "vue3",
        link: "/vue3/",
      },
      {
        text: "pinia相关",
        link: "/pinia/",
        children: [
          {
            text: "在普通js中使用pinia",
            link: "/pinia/inNormalJS/",
          },
        ],
      },
      {
        text: "npm",
        link: "/npm",
        children: [
          {
            text: "node-sass",
            link: "/npm/node-sass",
          },
        ],
      },
      {
        text: "微前端",
        link: "/microFrontEnd",
        children: [
          {
            text: "公共组件库",
            link: "/microFrontEnd/公共组件库",
          },
          {
            text: "静态资源加载的问题",
            link: "/microFrontEnd/静态资源加载的问题",
          },
        ],
      },
      {
        text: "nginx相关",
        link: "/nginx/",
        children: [
          {
            text: "https协议配置",
            link: "/nginx/httpsConfig/",
          },
          {
            text: "gzip压缩",
            link: "/nginx/gzip压缩/",
          },
        ],
      },
      {
        text: "rust",
        link: "/rust/",
      },
      {
        text: "vuepress",
        link: "/vuepress/",
      },
    ],
    repo: "https://gitee.com/cpf23333/blog",
    repoLabel: "博客源码",
    editLink: false,
    author: "cpf23333",
    displayFooter: true,
    footer: `<a href="https://beian.miit.gov.cn/#/Integrated/index" target="_blank">晋ICP备20007378号</a>`,
    plugins: {
      copyCode: {
        showInMobile: true,
      },
      blog: {},
      nprogress: true,
      git: {
        contributors: true,
        createdTime: true,
        updatedTime: true,
      },
      mdEnhance: {
        imgLazyload: true,
        mark: true,
        imgSize: true,
      },
      copyright: {},
    },
    // pure: true,
    themeColor: { 蓝色: "blue" },
  }),
})
