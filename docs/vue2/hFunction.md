# h 函数

首先，看一下 h 函数是个啥玩意

<https://github.com/vuejs/babel-plugin-transform-vue-jsx/issues/6>

h 函数其实就是 this.$createElement，它的作用是生成一个 vNode

我们的业务很可能会有一块内容逻辑很复杂，只靠 template 很难比较好地写出功能，这个时候就需要换成 jsx 的写法，提升代码的灵活性

那 jsx 是怎么被 vue 正常处理的？它又不是 template

是@babel/plugin-syntax-jsx 和@vue/babel-plugin-transform-vue-jsx

@vue/babel-plugin-transform-vue-jsx 在对 <code>.vue</code>和<code>.jsx</code> 文件内的代码进行转换时会在闭包内混入一个 h 函数，将 html 相关的代码转换成下面的形式

```javascript
h(
  "tagName",
  {
    props,
    slot,
    scopedSlots: { default: () => xxx, header: scope => scope.xxx },
    class: "",
  },
  [children1, children2]
)
```

在这个过程中，template 标签的内容和变量与函数的 jsx 代码一起被转换掉了

所以我们可以在 vue 文件或 render 函数内正常书写 jsx

那普通 js 文件里怎么写 jsx 呢，如何让一个普通变量成为一个 VNode 对象？这里没有 h 函数。

从 vue2.7 开始  h 函数被单独导出来了，我们只需要导入一下 h 函数即可

```javascript
import { h } from "vue"
```

那2.7之前的版本嘞

```javascript
import Vue from "vue"
let instance = new Vue()
export let h = instance.$createElement
```

普通 js 文件导入 h 这个变量，在编译时作用域内就会有这个 h 变量，于是 **@vue/babel-plugin-transform-vue-jsx** 处理出的 <code>h("tagName")</code> 代码就可以正常执行了
