# nginx 配置使用 https 协议

```nginx
# http服务自动跳转https用
server{
    listen 80;
    return 301 https://$server_name$request_uri;
}
# 实际的服务
server{
    listen 443 ssl default_server;
    listen [::]:443 ssl default_server;
    # SSL 协议版本
    ssl_protocols TLSv1.2;
    # 证书
    ssl_certificate 你证书下载下来的pem文件，相对绝对路径都可以;
    # 私钥
    ssl_certificate_key key文件;
    # ssl验证相关配置
    ssl_session_timeout  5m;    #缓存有效期
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;    #加密算法
    ssl_protocols TLSv1 TLSv1.1;    #安全链接可选的加密协议
    ssl_prefer_server_ciphers on;   #使用服务器端的首选算法
}

```
