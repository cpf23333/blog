# 在普通 js 文件中使用 pinia

pinia 的使用方法与旧版 vuex 不同，它不再定义一个巨大的仓库，把所有数据和操作函数放到根对象或各自 modules 里

而是允许定义多个具有唯一 id 的小型仓库，有一个根实例把所有的小仓库关联起来

```javascript
// store文件
import * as Pinia from "pinia"

export let pinia = Pinia.createPinia()

export let useInfo = Pinia.defineStore("info", {
  state: () => {
    return {
      name: "",
    }
  },
})
```

```javascript
// main.js
import { pinia } from "./store"

createApp(App).use(pinia).use(router).mount("#app")
```

```javascript
// 使用
import { useInfo, pinia } from "@/store"
let store = useInfo(pinia)
```

上面的代码在 vue 的 setup 内是没有问题的，但在普通的 js 内会报错

![pinia报错](./assets/pinia_err.png "pinia报错")

原因就是各个 pinia 小仓库未找到根实例，给各个仓库绑定根实例这个步骤是 vue 使用 inject 提供的
<https://pinia.web3doc.top/core-concepts/outside-component-usage.html#%E5%9C%A8%E7%BB%84%E4%BB%B6%E5%A4%96%E4%BD%BF%E7%94%A8%E5%AD%98%E5%82%A8>
<https://pinia.web3doc.top/ssr/#%E5%9C%A8-setup-%E4%B9%8B%E5%A4%96%E4%BD%BF%E7%94%A8%E5%AD%98%E5%82%A8>

上面的链接里 pinia 官方给了一个解决方案，但这样需要在每个路由进入之前都去处理一遍 store

可不可以这样？

```javascript
import * as Pinia from "pinia"

export let pinia = Pinia.createPinia()

let useInfo = Pinia.defineStore("info", {
  state: () => {
    return {
      name: "",
    }
  },
})
export let info = useInfo(pinia)
```
